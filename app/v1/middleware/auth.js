'use strict';
const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
const moment = require('moment');//moment for date time timezone handling
const mongoose = require('mongoose');
const user = mongoose.model("users");

//user authentication and redirection
router.use(async (req, res, next) => {
    try {
        let token = req.headers['token'];
        let device_id = req.headers['device_id'];
        let app_version = req.headers['app_version'];
        let language = req.headers['language'];
        if (!token) {
            return next({ status: 401, message: 'No token provided' });
        }
        jwt.verify(token, global.secret, {
            ignoreExpiration: true
        }, async (err, decoded) => {
            if (err) {
                return next({ status: 401, message: messages.sessionExpired });
            }

            try {
                req.decoded = decoded;
                let curentTimestamp = moment().utc().unix();
                let userData = await user.findOneAndUpdate({ _id: decoded._id }, { last_login: curentTimestamp, app_version: app_version,language});
                if (userData == null)
                    return next({ status: 401, message: messages.userAuthenticationFailed });
                if (userData.status === 0) {
                    return next({ message: messages.accountDisabledByAdmin, status: 401 });
                }

               

                //check if account is deleted by admin
                if (userData.is_deleted == true || userData.is_deleted == 1) {
                    return next({ message: messages.accountDeleted, status: 401 });
                }

                req.decoded.name = userData.name;
                req.decoded.contact_name = userData.contact_name;
                req.decoded.email = userData.email;
                req.decoded.phone = userData.phone;
                req.decoded.country_code = userData.country_code;
                req.decoded.profile_pic = userData.profile_pic;
                req.decoded.address = userData.address;
                req.decoded.verified_status = userData.verified_status;
                req.decoded.type = userData.type;
                return next();
            } catch (err) {
                return next({ message: err.message, status: 500 });
            }
        });
    } catch (err) {
        return next({ message: err.message, status: 500 });
    }
});

module.exports = router;
