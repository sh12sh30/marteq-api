'use strict';

const express = require('express');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const validator = require('express-validator');
//compression used to compress the data
const compression = require('compression');
//helmet module from preventing various attavck
const helmet = require('helmet');
var app = express();
//swig template engin is used
// const swig = require('swig');
var config = require('./config.js')

// set env veriables
process.env.PORT = config.env.server.port;
process.env.name = config.env.name;

//require connection file to connect mongo
const db = require('./connection');
db.connect();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

//logger for printing the logs
app.use(logger('dev'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
//compression module for compression
app.use(compression());
//helmet module for revent from attack
app.use(helmet());
app.use(helmet.referrerPolicy({ policy: 'same-origin' }));
app.use(express.static(path.join(__dirname, 'public')));
//express validator to validate the request
app.use(validator());

var server = require('http').Server(app);
const io = require('socket.io')(server);

io.on('connection', function(socket) {
    console.log("socket with id " + socket.id + " connected");
});

// makes io available as global.io in all request handlers
global.io = io;

// require routes
const onBoarding = require('./app/v1/user/onboarding-router');
const authMiddleware = require('./app/v1/middleware/auth');
const user = require('./app/v1/user/router');

//check for keys in headers
app.use((req, res, next) => {

    //device id
    if (!req.headers.device_id) {
        let err = { message: "Device Id is missing" };
        err.status = 400;
        return next(err);
    } else if (!req.headers.device_type) { //device type 1 for android and 2 for ios
        let err = { message: "Device type is missing" };
        err.status = 400;
        return next(err);
    } else if (!req.headers.app_version) { //app version currently used by user
        let err = { message: "app version is missing" };
        err.status = 400;
        return next(err);
    } 

    next();
});


//for onboarding process
app.use('/v1/', onBoarding);
//middleware for verification of token
app.use("/v1/*", authMiddleware);

//for user module
app.use('/v1/user', user);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.send({
        message: err.message,
        status: err.status
    });
});

module.exports = { app: app, server: server };